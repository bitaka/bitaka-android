package com.example.bitaka

import android.content.Intent
import android.media.MediaPlayer
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //when i click on json button, it opens the "activity" or the list view of all Cards
        //which i downloaded from the internet
        btn_sql.setOnClickListener() {
                // your code to update the UI thread here
                val intent = Intent(this, SQLActivity::class.java)
                startActivity(intent)
        }
        btn_json.setOnClickListener() {
            val intent = Intent(this, JsonActivity::class.java)
            startActivity(intent)
        }
        btn_flip.setOnClickListener(){
            val intent = Intent(this, FlipAnimation::class.java)
            startActivity(intent)
        }
        btn_record.setOnClickListener(){
            val intent = Intent(this, AudioAppActivity::class.java)
            startActivity(intent)
        }



    }
}
















