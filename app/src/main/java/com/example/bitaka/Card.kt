package com.example.bitaka

data class Card (var front: String, var back :String, var additionalInfo : String = "") {
    var id: Int = 0;
};
