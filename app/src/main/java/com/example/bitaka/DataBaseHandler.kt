

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.widget.Toast
import com.example.bitaka.SqlCard

/**
 * Created by VickY on 2017-11-28.
 */

const val DATABASE_NAME ="MyDB"
const val TABLE_NAME="Users"
const val COL_FRONT = "name"
const val COL_BACK = "age"
const val COL_ADDINFO = "gender"
const val COL_ID = "id"


class DataBaseHandler(var context: Context) : SQLiteOpenHelper(context,DATABASE_NAME,null,1){

    override fun onCreate(db: SQLiteDatabase?) {

        val createTable = "CREATE TABLE " + TABLE_NAME  +" ( " +
                COL_ID  +" INTEGER PRIMARY KEY, " +
                COL_FRONT  + " VARCHAR(256), " +
                COL_BACK  + " VARCHAR(256), " +
                COL_ADDINFO  + " VARCHAR(256))"




        db?.execSQL(createTable)

    }


    override fun onUpgrade(db: SQLiteDatabase?,oldVersion: Int,newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun insertData(sqlCard : SqlCard){
        val db = this.writableDatabase
        var cv = ContentValues()
        cv.put(COL_FRONT,sqlCard.sqlFront)
        cv.put(COL_BACK,sqlCard.sqlBack)
        cv.put(COL_ADDINFO,sqlCard.SqlAddInfo)
        var result = db.insert(TABLE_NAME,null,cv)
        if(result == -1.toLong())
            Toast.makeText(context,"Failed",Toast.LENGTH_SHORT).show()
        else
            Toast.makeText(context,"Success",Toast.LENGTH_SHORT).show()
    }
    fun getListContents(): Cursor? {
        val db = this.writableDatabase
        return db.rawQuery("SELECT * FROM $TABLE_NAME", null)
    }

    fun readData() : MutableList<SqlCard>{
        var list : MutableList<SqlCard> = ArrayList()

        val db = this.readableDatabase
        val query = "Select * from " + TABLE_NAME
        val result = db.rawQuery(query,null)
        if(result.moveToFirst()){
            do {
                var sqlCard = SqlCard()
                sqlCard.id = result.getString(result.getColumnIndex(COL_ID)).toInt()
                sqlCard.sqlFront = result.getString(result.getColumnIndex(COL_FRONT))
                sqlCard.sqlBack = result.getString(result.getColumnIndex(COL_BACK))
                sqlCard.SqlAddInfo = result.getString(result.getColumnIndex(COL_ADDINFO))

                list.add(sqlCard)
            }while (result.moveToNext())
        }

        result.close()
        db.close()
        return list
    }
    fun deleteData(){
        val db = this.writableDatabase
        db.delete(TABLE_NAME,null,null)
        db.close()
    }


    /*fun updateData() {
        val db = this.writableDatabase
        val query = "Select * from " + TABLE_NAME
        val result = db.rawQuery(query,null)
        if(result.moveToFirst()){
            do {
                var cv = ContentValues()
                cv.put(COL_BACK,(result.getString(2) + "sdw"))
                db.update(TABLE_NAME,cv,COL_ID + "=? AND " + COL_FRONT + "=? AND" + COL_ADDINFO+ "=?",
                    arrayOf(result.getString(result.getColumnIndex(COL_ID)),
                        result.getString(result.getColumnIndex(COL_FRONT)),result.getString(result.getColumnIndex(COL_ADDINFO))))
            }while (result.moveToNext())
        }

        result.close()
        db.close()
    }*/

}