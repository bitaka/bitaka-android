package com.example.bitaka

import DataBaseHandler
import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.android.synthetic.main.card_list_item.*
import kotlinx.android.synthetic.main.sql_activity.*

class SQLActivity : AppCompatActivity() {
    var isFabOpen: Boolean = false
    lateinit var fab: FloatingActionButton
    lateinit var fab1: FloatingActionButton
    lateinit var fab2: FloatingActionButton
    lateinit var fab3: FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.sql_activity)

        val context = this
        var db = DataBaseHandler(context)
        var data = db.readData()
        //CardItem has a card object and a String which is the absolute path for the record of the card
        val arrayList = ArrayList<CardItem>()
        //convert the data base into ArrayList of CardItem
        data.forEach { t ->
            arrayList.add(
                CardItem(
                    Environment.getExternalStorageDirectory().absolutePath + "/" + t.sqlFront + ".mp3",
                    Card(t.sqlFront, t.sqlBack, t.SqlAddInfo)
                )
            )
        }
        val listView = findViewById<ListView>(R.id.tvResult)
        val listItems = ArrayList<String>()
        listItems.addAll(arrayList.map { t -> t.bitaka.front })
        val adapter = CardAdapter(this, arrayList)
        listView.adapter = adapter


        fab = findViewById<FloatingActionButton>(R.id.fab0)
        fab1 = findViewById<FloatingActionButton>(R.id.fab1)
        fab2 = findViewById<FloatingActionButton>(R.id.fab2)
        fab3 = findViewById<FloatingActionButton>(R.id.fab3)
        fab1.isVisible = false
        fab2.isVisible = false
        fab3.isVisible = false
        fab1.isEnabled = false
        fab2.isEnabled = false
        fab3.isEnabled = false


        fab0.setOnClickListener { view ->
            if (isFabOpen) {
                closeFabMenu()
            } else {
                showFabMenu()
            }
        }
        fab1.setOnClickListener {
            val intent = Intent(this, AddCard::class.java)
            startActivity(intent)

        }
        fab2.setOnClickListener {
            db.deleteData()
            //i should implement a Snakcbar here to go back if i click on this not on purpose
            Toast.makeText(this, "All Cards are deleted!", Toast.LENGTH_SHORT)
                .show()
        }

        fab3.setOnClickListener {
            Toast.makeText(this, "Uploading the Cards isn't implemented yet!", Toast.LENGTH_SHORT)
                .show()
        }


        /*btn_insert.setOnClickListener({
            if (etvName.text.toString().length > 0 &&
                etvAge.text.toString().length > 0 &&
                etvGender.text.toString().length > 0) {
                var user = SqlCard(etvName.text.toString(),etvAge.text.toString(),etvGender.text.toString())
                db.insertData(user)
                arrayList.add(user)


                //insert new element
                //nd tell me adapter with the changes
                //so it won't update the whole listView once again
                listItems.add(user.sqlFront)
                adapter.notifyDataSetChanged();


                //set the texts empty
                 etvName.getText().clear();
                etvAge.getText().clear();
                etvGender.getText().clear();

            } else {
                Toast.makeText(context,"Please Fill All Data's",Toast.LENGTH_SHORT).show()
            }

        })*/
        //if i want to add a card i just click the floating action button and then i move to another actvity




        /*btn_read.setOnClickListener({


            val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, listItems)
            listView.adapter = adapter
            adapter.add(arrayList.get(arrayList.size -1).sqlFront)

        })*/
        //if i click on the item i should
        listView.setOnItemClickListener { _, view, position, _ ->
            val element = view as TextView
            val frontText = arrayList.get(position)?.bitaka.front
            if (element.text.equals(frontText)) {
                val backText = arrayList?.get(position)?.bitaka.back
                val additionalText = arrayList?.get(position)?.bitaka.additionalInfo
                element.text = backText + "\n" + additionalText
            } else {
                element.text = frontText
            }
        }

        /*  btn_update.setOnClickListener({
              db.updateData()
              btn_read.performClick()
          })

          btn_delete.setOnClickListener({
              db.deleteData()
              btn_read.performClick()
          })*/


    }

    private fun showFabMenu() {
        fab1.isVisible = true
        fab2.isVisible = true
        fab3.isVisible = true
        fab1.isEnabled = true
        fab2.isEnabled = true
        fab3.isEnabled = true
        isFabOpen = true
        fab1.animate().translationY(-resources.getDimension(R.dimen.standard_70))
        fab2.animate().translationY(-resources.getDimension(R.dimen.standard_140))
        fab3.animate().translationY(-resources.getDimension(R.dimen.standard_210))
    }

    private fun closeFabMenu() {
        isFabOpen = false
        fab1.animate().translationY(0F)
        fab2.animate().translationY(0F)
        fab3.animate().translationY(0F)
        fab1.isEnabled = false
        fab2.isEnabled = false
        fab3.isEnabled = false

    }
}