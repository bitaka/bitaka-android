package com.example.bitaka

import android.animation.AnimatorInflater
import android.animation.AnimatorSet
import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Movie
import android.media.MediaPlayer
import android.os.Environment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.LayoutRes
import androidx.core.graphics.toColor
import kotlinx.android.synthetic.main.activity_main.*
import java.io.IOException


class CardAdapter(private val context: Context,
                    private val dataSource: ArrayList<CardItem>) : BaseAdapter() {

    private val inflater: LayoutInflater
            = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    //1
    override fun getCount(): Int {
        return dataSource.size
    }

    //2
    override fun getItem(position: Int): Any {
        return dataSource[position]
    }

    //3
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    //4
    @SuppressLint("ResourceAsColor")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // Get view for row item
        val rowView = inflater.inflate(R.layout.card_list_item, parent, false)

        val recipe = getItem(position) as CardItem

        val view = rowView.findViewById(R.id.texts) as View
        val titleTextView = rowView.findViewById(R.id.text1) as TextView
        val titleTextView2 = rowView.findViewById(R.id.text2) as TextView
        view.setOnClickListener(){

           var mSetRightOut = AnimatorInflater.loadAnimator(context, R.animator.out_ani) as AnimatorSet
            var mSetLeftIn = AnimatorInflater.loadAnimator(context,  R.animator.in_ani) as AnimatorSet
            //here i decide what is the front side of the card
            //i should notice that it's here a text view so if i want to add another text view
            //i should change titleTextView to the LinearLayout that includes them both
            //and then manipulate it to change the texts inside it and also disable them
            var mCardFrontLayout = view
            var mCardBackLayout = view
            val distance = 8000

            var scale = context.resources.displayMetrics.density * distance
            mCardFrontLayout!!.cameraDistance = scale
            mCardBackLayout!!.cameraDistance = scale

            var mIsBackVisible = titleTextView.text.equals(recipe.bitaka.back)
            if (!mIsBackVisible) {
                titleTextView.text = recipe.bitaka.back
                titleTextView2.text = recipe.bitaka.additionalInfo
                mSetRightOut?.setTarget(mCardFrontLayout);
                mSetLeftIn?.setTarget(mCardBackLayout);
                mSetRightOut?.start();
                mSetLeftIn?.start();
                mIsBackVisible = true;
            } else {
                titleTextView.text = recipe.bitaka.front
                titleTextView2.isEnabled = false
                mSetRightOut?.setTarget(mCardBackLayout);
                mSetLeftIn?.setTarget(mCardFrontLayout);
                mSetRightOut?.start();
                mSetLeftIn?.start();
                mIsBackVisible = false;

            }



            /*val frontText = recipe.bitaka.front
            if(titleTextView.text.equals(frontText)) {
                val backText = recipe.bitaka.back
                val additionalText = recipe.bitaka.additionalInfo
                titleTextView.text = backText +"\n"+ additionalText
            }
            else {
                titleTextView.text = frontText
            }*/
        }
// Get subtitle element
        val button = rowView.findViewById<ImageView>(R.id.btn_speak)


        button.setOnClickListener(){

            try{
                var mediaPlayer = MediaPlayer()
                mediaPlayer?.setDataSource(recipe.record)
                mediaPlayer?.prepare()
                mediaPlayer?.start()
                mediaPlayer.start()
            }
            catch(e: IOException){
                Toast.makeText(context, "there is now audio file!", Toast.LENGTH_SHORT).show()
            }


        }


// 2
        titleTextView.text = recipe.bitaka.front


// 3
       // Picasso.with(context).load(recipe.imageUrl).placeholder(R.mipmap.ic_launcher).into(thumbnailImageView)

        return rowView
    }




}