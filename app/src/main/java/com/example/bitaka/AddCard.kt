package com.example.bitaka


import DataBaseHandler
import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.media.MediaPlayer
import android.media.MediaRecorder
import android.os.Bundle
import android.os.Environment
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_audio_app.*
import kotlinx.android.synthetic.main.add_card.*
import java.io.IOException

class AddCard : AppCompatActivity() {
    private var output: String? = null
    private var mediaRecorder: MediaRecorder? = null
    private var state: Boolean = false
    private var recordingStopped: Boolean = false
    private var mediaPlayer: MediaPlayer? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.add_card)


        mediaRecorder = MediaRecorder()


        var nameOfTheRecord = ""
        var db = DataBaseHandler(this)
        btn_insert.setOnClickListener({
            if (etvName.text.toString().length > 0 &&
                etvAge.text.toString().length > 0
            ) {
                var user = SqlCard(
                    etvName.text.toString(),
                    etvAge.text.toString(),
                    etvGender.text.toString()
                )
                db.insertData(user)


                //insert new element
                //nd tell me adapter with the changes
                //so it won't update the whole listView once again
                //set the texts empty
                etvName.getText().clear();
                etvAge.getText().clear();
                etvGender.getText().clear();
                nameOfTheRecord = user.sqlFront
            } else {
                Toast.makeText(this, "Please Fill All Data's", Toast.LENGTH_SHORT).show()
            }


        })
        /*btn_record.setOnClickListener {
         //the idea here is that i enter the card's data at first
            // and then i use the front part to name the record after it
            //so i should not have two identical cards
            //and here i ask at first if the nameOfTheRecord is an empty string or not
            //if it is , i should enter the card's data first
            //and then i can start recording
            if(nameOfTheRecord==""){
                Toast.makeText(this,"Enter the Card's first!", Toast.LENGTH_SHORT).show()
            }
            else {
                if (ContextCompat.checkSelfPermission(
                        this,
                        Manifest.permission.RECORD_AUDIO
                    ) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                        this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ) != PackageManager.PERMISSION_GRANTED
                ) {


                    val permissions = arrayOf(
                        android.Manifest.permission.RECORD_AUDIO,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE
                    )
                    ActivityCompat.requestPermissions(this, permissions, 0)
                } else {
                    output = Environment.getExternalStorageDirectory().absolutePath +"/"+ nameOfTheRecord+"mp3"

                    mediaRecorder?.setAudioSource(MediaRecorder.AudioSource.MIC)
                    mediaRecorder?.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
                    mediaRecorder?.setAudioEncoder(MediaRecorder.AudioEncoder.AAC)
                    mediaRecorder?.setOutputFile(output)
                    startRecording()
                    nameOfTheRecord = ""

                }
            }
            /*val intent = Intent(this, SQLActivity::class.java)
            startActivity(intent)*/
        }*/
        btn_record.setOnTouchListener { v: View, m: MotionEvent ->
            // Perform tasks here
            var beginOfRecording: Long = 0;
            var endOfRecording: Long = 0
            val action = m.action
            beginOfRecording = System.currentTimeMillis();
            when (action) {
                MotionEvent.ACTION_DOWN -> {
                    //record like in whatsApp
                    //or take the write measures

                    ////////////////////////
                    //the idea here is that i enter the card's data at first
                    // and then i use the front part to name the record after it
                    //so i should not have two identical cards
                    //and here i ask at first if the nameOfTheRecord is an empty string or not
                    //if it is , i should enter the card's data first
                    //and then i can start recording
                    if (nameOfTheRecord == "") {
                        Toast.makeText(this, "Enter the Card's first!", Toast.LENGTH_SHORT).show()
                    } else {
                        if (ContextCompat.checkSelfPermission(
                                this,
                                Manifest.permission.RECORD_AUDIO
                            ) != PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
                                this,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE
                            ) != PackageManager.PERMISSION_GRANTED
                        ) {


                            val permissions = arrayOf(
                                android.Manifest.permission.RECORD_AUDIO,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE
                            )
                            ActivityCompat.requestPermissions(this, permissions, 0)
                        } else {

                            output =
                                Environment.getExternalStorageDirectory().absolutePath + "/" + nameOfTheRecord + ".mp3"

                            mediaRecorder?.setAudioSource(MediaRecorder.AudioSource.MIC)
                            mediaRecorder?.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4)
                            mediaRecorder?.setAudioEncoder(MediaRecorder.AudioEncoder.AAC)
                            mediaRecorder?.setOutputFile(output)
                            startRecording()
                        }
                    }

                }
                MotionEvent.ACTION_UP -> {
                    //here after stop clicking i should move in a perfect situation back to the first sql activity
                    if (nameOfTheRecord == "") {
                        //if i want to record before entering the data
                        Toast.makeText(this, "Enter the Card's first!", Toast.LENGTH_SHORT).show()
                    } else {
                        //done
                        //i should return to the card list
                        var endOfRecording = System.currentTimeMillis()
                        stopRecording()
                        //but to avoid fast clicking so trivial recording, all records should be at least 50 milli second
                        if (endOfRecording - beginOfRecording >= 500) {
                            Toast.makeText(this, "going back to your list!", Toast.LENGTH_SHORT)
                                .show()
                            val intent = Intent(this, SQLActivity::class.java)
                            startActivity(intent)
                            nameOfTheRecord = ""
                        } else {
                            //if they are not 50 i shouldn't go to the activity but i should record once a gain
                            Toast.makeText(this, "I'm not hearing anything!", Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                }

            }
            true
        }

    }

    private fun startRecording() {
        try {
            mediaRecorder?.prepare()
            mediaRecorder?.start()
            state = true
            Toast.makeText(this, "Recording started!", Toast.LENGTH_SHORT).show()
        } catch (e: IllegalStateException) {
            e.printStackTrace()
            Toast.makeText(this, "IllegalStateException!", Toast.LENGTH_SHORT).show()
        } catch (e: IOException) {
            e.printStackTrace()
            Toast.makeText(this, "IOException!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun stopRecording() {
        if (state) {
            mediaRecorder?.stop()
            mediaRecorder?.release()
            state = false
        } else {
            Toast.makeText(this, "You are not recording right now!", Toast.LENGTH_SHORT).show()
        }
    }
}