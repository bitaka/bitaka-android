package com.example.bitaka


import android.animation.AnimatorInflater
import android.animation.AnimatorSet
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.animation_activity.*


class FlipAnimation : AppCompatActivity() {

    var mIsBackVisible = false
    lateinit var  mCardFrontLayout: View
    lateinit var  mCardBackLayout: View
    lateinit private  var mSetRightOut: AnimatorSet
    lateinit private var mSetLeftIn: AnimatorSet

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.animation_activity)

       findViews();
        loadAnimations();
        changeCameraDistance();
    }
   private fun loadAnimations() {
        mSetRightOut = AnimatorInflater.loadAnimator(this, R.animator.out_ani) as AnimatorSet
        mSetLeftIn = AnimatorInflater.loadAnimator(this,  R.animator.in_ani) as AnimatorSet
    }

    private fun findViews() {
        mCardBackLayout = card_back
        mCardFrontLayout = card_front
    }


    private fun changeCameraDistance() {
        val distance = 8000
        val scale = resources.displayMetrics.density * distance
        mCardFrontLayout!!.cameraDistance = scale
        mCardBackLayout!!.cameraDistance = scale
    }


    fun flipCard(view : View) {
        if (!mIsBackVisible) {
            mSetRightOut?.setTarget(mCardFrontLayout);
            mSetLeftIn?.setTarget(mCardBackLayout);
            mSetRightOut?.start();
            mSetLeftIn?.start();
            mIsBackVisible = true;
        } else {
            mSetRightOut?.setTarget(mCardBackLayout);
            mSetLeftIn?.setTarget(mCardFrontLayout);
            mSetRightOut?.start();
            mSetLeftIn?.start();
            mIsBackVisible = false;
        }

    }
}